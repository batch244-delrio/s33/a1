//  Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((response) => console.log(response));

//  Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.

	

fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => {

    let list = json.map((todo => {
        return todo.title;
    }))

    console.log(list);
});

// Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => console.log (`The item "${json.title}" on the list has a status of ${json.completed}`));

// Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API

// [Section] Creating a to do list item using POST method
fetch('https://jsonplaceholder.typicode.com/todos', {
    method: 'POST',
    headers: {
    'Content-type': 'application/json',
    },
    body: JSON.stringify({
        title: 'Created To Do List Item',
        completed: false,
        userId: 1
    })
})
.then((response) => response.json())
.then((json) => console.log(json));

// [Section] Updating a to do list item using PUT method
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PUT',
    headers: {
    'Content-type': 'application/json',
    },
    body: JSON.stringify({
        title: 'Updated To Do List Item',
        description: 'To update the my to do list with a different data structure.',
        status: 'Pending',
        dateCompleted: 'Pending',
        userId: 1
    })
})
.then((response) => response.json())
.then((json) => console.log(json));

// [Section] Updating a to do list item using PATCH method
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PATCH',
    headers: {
    'Content-type': 'application/json',
    },
    body: JSON.stringify({
        status: 'Complete',
        dateCompleted: '07/09/21'
    })
})
.then((response) => response.json())
.then((json) => console.log(json));

// [Section] Deleting a to do list item
fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'DELETE',
});